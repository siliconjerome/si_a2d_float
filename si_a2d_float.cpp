#include "si_a2d_float.h"

#define PUT 0
#define GET 1


/////////////////////////////////////////////////////////////////////
//name: mapf
//author: jerome verhoeven
//date: 11/17/2014
//desc: maps in the input x to the line defined by in_min, in_max,
//      out_min and out max.
//args: x -> the value that needs to be mapped
//      in_min, in_max, out_min, out_max -> define the line
//ret: float of x mapped to an output.
//notes: there are no constrains to the input being within the
//      mapping value.
/////////////////////////////////////////////////////////////////////
float mapf(float x, float in_min, float in_max, float out_min, float out_max)
{
    if((in_max - in_min) == 0) return 0;
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/////////////////////////////////////////////////////////////////////
//name: a2d
//author: jerome verhoeven
//date: 2014.11.17
//desc: pushes a value into the a2d buffer (this can be
//      bypassed) and returns the average a2d value
//args: pointer to Thermistor object,
//      action 0 means sample a2d; 1 means just return the avg value
//      val: a2d value
//ret: int average a2d value
//notes:
/////////////////////////////////////////////////////////////////////
int Si_a2d_float2_a2d(struct Si_a2d_float2 *a, int action, int val)
{
    //static int a2d[10];
    //static int index = 0;
    long sum = a->buffer_count / 2; //rounding
    int i = 0;

    if(action == PUT)
    {
        a->buffer[a->buffer_index] = val;
        a->buffer_index++;
        if(a->buffer_index >= a->buffer_count) a->buffer_index = 0;
    }

    for(i = 0; i < a->buffer_count; i++)
    {
        sum += (long)a->buffer[i];
    }

    return (int)(sum / a->buffer_count);
}

/////////////////////////////////////////////////////////////////////
//name: Si_a2d_float2_tick
//author: jerome verhoeven
//date: 2014.11.17
//desc: actions to be performed periodically on this module.
//args: pointer to Thermistor struct
//ret:
//notes:
/////////////////////////////////////////////////////////////////////
int Si_a2d_float2_tick(struct Si_a2d_float2 * t, int val)
{
    Si_a2d_float2_a2d(t, PUT, val);
    return 0;
}

/////////////////////////////////////////////////////////////////////
//name: push
//author: jerome verhoeven
//date: 2015.10.14
//desc: pushes a value into the buffer
//args: value to push into the buffer
//ret: int buffer index
//notes:
/////////////////////////////////////////////////////////////////////
int si_a2d_float::push(int val)
{
  if(!valid_flag) return 0;
  buffer[buffer_index] = val;
  buffer_index++;
  if(buffer_index > buffer_count) buffer_index = 0;
  return buffer_index;
}

/////////////////////////////////////////////////////////////////////
//name: Si_a2d_float2_get
//author: jerome verhoeven
//date: 2014.11.17
//desc: get the float value associated with the a2d value
//args: pointer to the Si_a2d_float2 struct
//ret: float
//notes:
/////////////////////////////////////////////////////////////////////
float Si_a2d_float2_get(struct Si_a2d_float2 *t)
{
    int a2d = 0;
    float ret_val = 0;
    int i = 0;
    struct Si_a2d_float2_pt *my_map = t->map;


    a2d = Si_a2d_float2_a2d(t, GET, 0);



    while(a2d > my_map[i].a2d && i < t->map_count)
    {
        i++;
    }
    if(i == t->map_count) i--;
    else if(i == 0) i++;


    ret_val = mapf((float)a2d, (float)my_map[i-1].a2d, (float)my_map[i].a2d,
                                my_map[i-1].val, my_map[i].val);

    #ifdef DEBUG_SI_A2D_FLOAT
    Serial.print("a2d is: ");
    Serial.println(a2d);
    Serial.print("map_count is: ");
    Serial.println(t->map_count);
    Serial.println("map values are...");
    for(int j = 0; j < t->map_count; j++)
    {
        Serial.print("{");
        Serial.print(my_map[j].a2d);
        Serial.print(", ");
        Serial.print(my_map[j].val);
        Serial.println("}");
    }
    Serial.print("i is: ");
    Serial.println(i);
    Serial.print("return value is: ");
    Serial.println(ret_val);
    #endif

    return ret_val;


}

/////////////////////////////////////////////////////////////////////
//name: get
//author: jerome verhoeven
//date: 2015.10.14
//desc: get the float value associated with the a2d value
//args: none
//ret: float
//notes:
/////////////////////////////////////////////////////////////////////
float si_a2d_float::get()
{
  if(!valid_flag) return (float)0;
  long sum_of_buffer = buffer_count / 2; //for rounding
  int avg_buffer_value = 0;
  int i = 0;
  float ret_value = 0;
  //sum the buffer
  for(i = 0; i < buffer_count; i++)
  {
      sum_of_buffer += (long)buffer[i];
  }

  avg_buffer_value = (int)(sum_of_buffer / buffer_count);

  i = 0;
  while(avg_buffer_value > map[i].a2d && i < map_count)
  {
      i++;
  }
  if(i == map_count) i--;
  else if(i == 0) i++;


  ret_value = mapf((float)avg_buffer_value, (float)map[i-1].a2d, (float)map[i].a2d,
                              map[i-1].val, map[i].val);

  return ret_value;
}

/////////////////////////////////////////////////////////////////////
//name: Si_a2d_float2_init
//author: jerome verhoeven
//date: 2014.11.17
//desc: initialize the Si_a2d_float2 struct
//args:
//      a: pointer to  struct
//      map: pointer the the map [a2d, float value]
//      map_count: number of entries in the map.
//      buf: pointer to buffer to keep a2d values in
//      buf_max: size of the buffer
//ret: 0
//notes:
/////////////////////////////////////////////////////////////////////
int Si_a2d_float2_init(struct Si_a2d_float2 * a,
                    struct Si_a2d_float2_pt * map,
                    int map_count,
                    int *buf,
                    int buf_max)
{
    a->tick = Si_a2d_float2_tick;
    a->get = Si_a2d_float2_get;
    a->mapf = mapf;
    a->map = map;
    a->map_count = map_count;
    a->buffer = buf;
    a->buffer_count = buf_max;
    a->buffer_index = 0;

    //pinMode(a->pin, INPUT);

    return 0;
}

///////////////////////////////////////////////////////////////////////////////
//name: is_valid
//desc: returns if the object was constructed properly
//args: none
//retu: true for yes; false for no
//auth: jerome verhoeven
//date: 2015.10.14
//note: none
///////////////////////////////////////////////////////////////////////////////
bool si_a2d_float::is_valid()
{
  return valid_flag;
}

/////////////////////////////////////////////////////////////////////
//name: Si_a2d_float
//author: jerome verhoeven
//date: 2015.10.14
//desc: initialize the Si_a2d_float struct
//args:
//      a: pointer to  struct
//      map: pointer the the map [a2d, float value]
//      map_count: number of entries in the map.
//      buf: pointer to buffer to keep a2d values in
//      buf_max: size of the buffer
//ret: 0
//notes:
/////////////////////////////////////////////////////////////////////
si_a2d_float::si_a2d_float(struct si_a2d_float_pt* a2d_map, int a2d_map_cnt, int* a2d_buf, int a2d_buf_cnt)
{
  if((!a2d_map) || (a2d_map_cnt < 2) || (!a2d_buf) || (a2d_map_cnt < 1))
  {
    valid_flag = false;
    return;
  }
  map = a2d_map;
  map_count = a2d_map_cnt;
  buffer = a2d_buf;
  buffer_count = a2d_buf_cnt;
  buffer_index = 0;
  valid_flag = true;
  return;
}
