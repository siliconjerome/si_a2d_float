#ifndef APPLICATION_H
  #define APPLICATION_H
  #include "application.h"
#endif

#ifndef SI_A2D_FLOAT_H
  #define SI_A2D_FLOAT_H
  #include "si_a2d_float.h"
#endif

#ifndef STDLIB_H
  #define STDLIB_H
  #include <stdlib.h>
#endif

int cnt = 0;
int cnt2 = 0;
int num = 0;
int i = 0;
int j = 0;
int k = 0;
char c;
char buf[20] = {0};
// map of 1
struct si_a2d_float_pt map1[1] = {0,500};
//map of 2
struct si_a2d_float_pt map2[2] = {{100, 1}
                                  ,{200, 2}};
//map of 3
struct si_a2d_float_pt map3[3] = {{100, -1}
                                  ,{200, 0}
                                  ,{300, 1}};
//map of 10
struct si_a2d_float_pt map10[10] = {{-200, -2}
                                  ,{-100, -1}
                                  ,{0, 0}
                                  ,{100, 1}
                                  ,{200,2}
                                  ,{300, 3}
                                  ,{400, 4}
                                  ,{500, 5}
                                  ,{600, 6}
                                  ,{700, 7}};
int buffer1[1];
int buffer100[100];
int buffer1000[1000];

si_a2d_float map1buf1(map1, 1, buffer1, 1);
si_a2d_float map2buf1(map2, 2, buffer1, 1);
si_a2d_float map2buf100(map2, 2, buffer100, 100);
si_a2d_float map3buf100(map3, 3, buffer100, 100);
si_a2d_float map10buf100(map10, 10, buffer100, 100);
si_a2d_float map10buf1000(map10, 10, buffer1000, 1000);

//prototypes
bool check_constructor(si_a2d_float);
bool run_test(si_a2d_float);

void setup()
{
  Serial.begin(9600);
  Serial.println("press any key to start");
  while(!Serial.available());
  Serial.print("input first int to fill buffer's with: ");
}

void loop()
{

    c = Serial.read();
    if((c >= 0x30 && c <=0x39) || (c == '-'))
    {
      buf[cnt] = c;
      cnt++;
      if(cnt > 19) cnt = 0;
    }
    if(c == 0x0A)
    {
      //Serial.println(buf);
      num = atoi(buf);
      if(cnt2 == 0)
      {
        i = num;
        Serial.print("i = ");
      }
      if(cnt2 == 1)
      {
        j = num;
        Serial.print("j = ");
      }
      if(cnt2 == 2)
      {
        k = num;
        Serial.print("k = ");
      }
      cnt2++;
      //if(cnt2 > 2)cnt2 = 0;
      Serial.println(num);
      cnt = 0;
      for(cnt = 0; cnt < 20; cnt++) buf[cnt] = 0;
      cnt = 0;
    }
    if(cnt2 == 3)
    {
      Serial.println("filling the buffers");
      while(cnt < 2000)
      {
        map1buf1.push(i);
        map2buf1.push(i);
        map2buf100.push(i);
        map3buf100.push(i);
        map10buf100.push(i);
        map10buf1000.push(i);
        map1buf1.push(j);
        map2buf1.push(j);
        map2buf100.push(j);
        map3buf100.push(j);
        map10buf100.push(j);
        map10buf1000.push(j);
        map1buf1.push(k);
        map2buf1.push(k);
        map2buf100.push(k);
        map3buf100.push(k);
        map10buf100.push(k);
        map10buf1000.push(k);
        cnt++;
      }
      cnt = 0;
      Serial.println("start test");
      run_test(map1buf1);
      run_test(map2buf1);
      run_test(map3buf100);
      run_test(map10buf100);
      run_test(map10buf1000);
      cnt2 = 0;
    }



    //delay(5000);
}
//test creatation of a2d object

//test push
  //test filling the buffer and wrap around
  //test passing invalid value
  //check return value
//test get
  //check 1 and 2 count maps
  //check rounding

///////////////////////////////////////////////////////////////////////////////
//name: check_constructor
//desc: checks object for proper construction
//args: si_a2d_float object
//rets: bool false for bad, true for good
///////////////////////////////////////////////////////////////////////////////
bool check_constructor(si_a2d_float obj)
{
  return obj.is_valid();
}

float check_value(si_a2d_float obj)
{
  return obj.get();
}

////////////////////////////////////////////////////////////////////////////////
//name: run_test
//desc: all tests that are run on this library
//args: si_a2d_float object
//rets: bool false for bad, true for good
///////////////////////////////////////////////////////////////////////////////
bool run_test(si_a2d_float obj)
{

  Serial.print("map count: ");
  Serial.print(obj.map_count);
  Serial.print(" | buffer count: ");
  Serial.println(obj.buffer_count);

  Serial.print("Construction test           :");
  if(check_constructor(obj)) Serial.println("pass");
  else Serial.println("fail");

  Serial.print("value                       :");
  Serial.println(check_value(obj));
}
