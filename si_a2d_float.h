#ifndef APPLICATION_H
  #define APPLICATION_H
  #include "application.h"
#endif
//#define DEBUG_SI_A2D_FLOAT

//correlates an a2d value with a temperature F
struct Si_a2d_float2_pt{
    int a2d;
    float val;
};

struct Si_a2d_float2{
    int(*tick)(struct Si_a2d_float2 *, int val);
    float(*get)(struct Si_a2d_float2 *);
    float(*mapf)(float x, float in_min, float in_max, float out_min, float out_max);
    struct Si_a2d_float2_pt *map;
    int map_count; // number of entries in the map
    int *buffer; //buffer of a2d values
    int buffer_count; //number of entries in the buffer
    int buffer_index; //current position of the index
};


int Si_a2d_float2_init(struct Si_a2d_float2*, struct Si_a2d_float2_pt*, int, int*, int);
//                    \__________________/  \_____________________/  \_/  \__/  \_/
//                                |                         |         |     |    |
//ptr to self --------------------+                         |         |     |    |
//ptr to map [a2d, float]-----------------------------------+         |     |    |
//number of map elements----------------------------------------------+     |    |
//ptr to buffer (for averaging)---------------------------------------------+    |
//size of buffer (number of elements)--------------------------------------------+
float mapf(float x, float in_min, float in_max, float out_min, float out_max);

/* <- remove to see color coding
///////////////////////////////////////////////////////////////////////////////
//HOW TO USE THIS LIBRARY//////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//VARIABLES
//these are variables and defines needed by the init statement
#define BUFFER_COUNT 100 //define the number of samples kept in the buffer
int sensor_buffer[BUFFER_COUNT]; //declare a buffer of that size
struct Si_a2d_float sensor;//declare a struct for the a2d object
#define MAP_COUNT 4 //define the number of elements in the map
                          //of a2d value -> float value


//declare and initialize the map.  The .get method will use linear extrapolation
//between mapped points, the a2d values must be in assending order, but the
//float values don't
struct Si_a2d_float_pt sensor_map[MAP_COUNT] = {{0, -12.1},
                                                {255, 32.4},
                                                {750, 1045},
                                                {1000, 750}};

//SET-UP run this once
Si_a2d_float_init(&sensor, sensor_map, MAP_COUNT, sensor_buffer, BUFFER_COUNT);

//IN YOUR PROGRAM
//run once for every sample of the a2d
sensor.tick(&sensor, analogRead(SENSOR_PIN));

//returns mapped float value using linear extrapolation
sensor.get(&sensor)
///////////////////////////////////////////////////////////////////////////////
//END HOW TO USE
///////////////////////////////////////////////////////////////////////////////
*/

struct si_a2d_float_pt{
    int a2d;
    float val;
};

///////////////////////////////////////////////////////////////////////////////
//name: Si_a2d_float
//desc: this class holds the data and methods for the easy way to map a2d to
//      float values
///////////////////////////////////////////////////////////////////////////////
class si_a2d_float{
public:
  struct si_a2d_float_pt* map;
  int map_count;
  int *buffer;
  int buffer_count;
  int buffer_index;
  bool valid_flag;
  int push(int val);
  float get();
  bool is_valid();
  si_a2d_float(struct si_a2d_float_pt*, int, int*, int);
};
